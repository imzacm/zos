#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/ZOS.kernel isodir/boot/ZOS.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "ZOS" {
	multiboot /boot/ZOS.kernel
}
EOF
grub-mkrescue -o ZOS.iso isodir
